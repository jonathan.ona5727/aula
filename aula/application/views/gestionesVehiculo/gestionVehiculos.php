<?php
foreach($css_files as $file): ?>
<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />

<?php endforeach; ?>


<?php foreach($js_files as $file): ?>

<script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>

   <div class="row" style="padding:0px !important; margin:0px !important;">
      <div class="col-md-12 text-center">
         <div class="section-title" data-aos="fade-right">
            <div class="well">
                  <h3><b>GESTIÓN DE VEHICULOS</b> </h3>
              </div>
            </div>
         </div>
      </div>

   <div data-aos="fade-left" data-aos-delay="200">
     <!-- //linea muy importate para presentar la tabla -->
      <?php echo $output; ?>
   </div>


</div>
