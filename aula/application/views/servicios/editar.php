<div class="row">
  <div class="col-md-12 text-center">
    <legend>
        <i class="glyphicon glyphicon-plus"></i>
        ACTUALIZAR SERVICIO
    </legend>
  </div>
<div class="row">
  <div class="col-md-3">

  </div>
  <div class="col-md-6">
    <form class="" action="<?php echo site_url(); ?>/servicios/actualizarServicio" method="post" id="frm_editar_servicio">
      <table class="table">
        <input type="hidden" name="id_ser" id="id_ser" class="form-control"
    value="<?php echo $servicioEditar->id_ser; ?>" placeholder="Ingrese su id">
        <!-- <tr>
          <td><label for="">Placa </label></td>
          <td><select name="fk_id_veh" id="fk_id_veh" class="form-control"
          value="" placeholder="Escoja la Placa" autocomplete="off">
          <option value="">--Seleccione--</option>
          <?php if ($listadoVehiculos): ?>
            <?php foreach ($listadoVehiculos->result() as $vehiculoTemporal): ?>
              <option value="<?php echo $vehiculoTemporal->id_veh; ?>">
                <?php echo $vehiculoTemporal->placa_veh; ?>
              </option>
            <?php endforeach; ?>
          <?php endif; ?>
      </select>
        </td>
        </tr> -->
        <tr>
          <td><label for="">Hora de Ingeso:</label></td>
          <td><input type="time" name="horaIngreso_ser" id="horaIngreso_ser" class="form-control"
          value="<?php echo $servicioEditar->horaIngreso_ser; ?>" placeholder="Ingrese la hora de Ingreso" required autocomplete="off"></td>
        </tr>
        <tr>
          <td><label for="">Hora de Salida:</label></td>
          <td><input type="time" name="horaSalida_ser" id="horaSalida_ser" class="form-control"
          value="<?php echo $servicioEditar->horaSalida_ser; ?>" placeholder="Ingrese la hora de Salida" required autocomplete="off"></td>
        </tr>

      </table>
      <button type="submit" name="button" class="btn btn-success">
        <i class="glyphicon glyphicon-ok"></i>
        Guardar</button>
      <a href="<?php echo site_url(); ?>/servicios/index" class="btn btn-danger">
        <i class="glyphicon glyphicon-remove"></i>
        Cancelar</a>
    </form>
  </div>
  <div class="col-md-3">

  </div>
</div>
</div>

<!-- <script type="text/javascript">
    $("#fk_id_veh").val('<?php echo $servicioEditar->fk_id_veh; ?>');
  </script> -->
<script type="text/javascript">
  $("#frm_editar_servicio").validate({
    rules:{
      // fk_id_veh:{
      //   required:true
      // },
      horaIngreso_ser:{
        required:true
      },
      horaSalida_ser:{
        required:true
      }
    },
    messages:{
      // fk_id_veh:{
      //   required:"Por escoja la Placa"
      // },
      horaIngreso_ser:{
        required:"Por favor ingrese la hora de Ingreso del vehiculo"
      },
      horaSalida_ser:{
        required:"Por favor ingrese la hora de Salida del vehiculo"
      }
    }
  });
</script>
