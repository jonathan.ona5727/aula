<div class="row">
  <div class="col-md-12 text-center">
    <legend>
        <i class="glyphicon glyphicon-plus"></i>
        EDITAR CLIENTE
    </legend>
  </div>
<div class="row">
  <div class="col-md-3">

  </div>
  <div class="col-md-6">
    <form class="" action="<?php echo site_url(); ?>/clientes/actualizarCliente" method="post" id="frm_editar_cliente">
      <input type="hidden" name="id_cli" id="id_cli" class="form-control" value="<?php echo $clienteEditar->id_cli; ?>" placeholder="Ingrese su id"><br>
      <input type="hidden" name="id_imagen"  id="id_imagen" class="form-control bg-light border-0"
  value="<?php echo $clienteEditar->imagen_cli;?>">
      <table class="table">
        <tr>
          <td><label for="">Imganen:</label></td>
          <td><input type="file" name="imagen_cli" id="imagen_cli" class="form-control"
          accept="image/jpeg" value="<?php echo $clienteEditar->nombre_cli; ?>" placeholder="Ingrese la imagen del medico"  ></td>
        </tr>
        <tr>
          <td><label for="">Cedula: </label></td>
          <td><input type="text" name="cedula_cli" id="cedula_cli" class="form-control"
            placeholder="Ingrese su nombre" required autocomplete="off" value="<?php echo $clienteEditar->nombre_cli; ?>" placeholder="Ingrese sus nombres"></td>
        </tr>
        <tr>
          <td><label for="">Nombres: </label></td>
          <td><input type="text" name="nombre_cli" id="nombre_cli" class="form-control"
            placeholder="Ingrese su nombre" required autocomplete="off" value="<?php echo $clienteEditar->nombre_cli; ?>" placeholder="Ingrese sus nombres"></td>
        </tr>
        <tr>
          <td><label for="">Apellidos:</label></td>
          <td><input type="text" name="apellido_cli" id="apellido_cli"  class="form-control bg-light border-0"
          value="<?php echo $clienteEditar->apellido_cli; ?>" placeholder="Ingrese sus apellidos" required autocomplete="off"></td>
        </tr>
        <tr>
          <td><label for="">Género:</label></td>
          <td><select name="genero_cli" id="genero_cli" class="form-control"
          value="" placeholder="Escoja su genero" required>
          <option value="">---Seleccione---</option>
          <option value="Masculino">Masculino</option>
          <option value="Femenino">Femenino</option>
        </select>
        </td>
        </tr>
      </table>
      <button type="submit" name="button" class="btn btn-success">
        <i class="glyphicon glyphicon-ok"></i>
        Guardar</button>
      <a href="<?php echo site_url(); ?>/clientes/index" class="btn btn-danger">
        <i class="glyphicon glyphicon-remove"></i>
        Cancelar</a>
    </form>
  </div>
  <div class="col-md-3">

  </div>
</div>
</div>
<script type="text/javascript">
    $("#genero_cli").val('<?php echo $clienteEditar->genero_cli; ?>');
  </script>
<script type="text/javascript">
  $("#frm_editar_cliente").validate({
    rules:{
      nombre_cli:{
        required:true
      },
      apellido_cli:{
        required:true
      },
      genero_cli:{
        required:true
      }
    },
    messages:{
      nombre_cli:{
        required:"Por favor ingrese los nombres"
      },
      apellido_cli:{
        required:"Por favor ingrese los apellidos"
      },
      genero_cli:{
        required:"Por favor escoga el genero"
      }
    }
  });
</script>
<script type="text/javascript">
  $(document).ready(function (){
    $("#imagen_cli").fileinput({
      language:'es'
    });
  });
</script>
