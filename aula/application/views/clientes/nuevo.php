<section>
<link rel="stylesheet" href="<?php echo base_url(); ?>/assets\Librerias\bootstrap 3/css/bootstrap.css">
<script type="text/javascript" src="<?php echo base_url(); ?>/assets\Librerias\bootstrap 3/js/bootstrap.js"></script>
</section>
<div class="row">
  <div class="col-md-12 text-center">
    <legend>
        <i class="glyphicon glyphicon-plus"></i>
        NUEVO CLIENTE
    </legend>
  </div>
<div class="row">
  <div class="col-md-3">

  </div>
  <div class="col-md-6">
    <form class="" action="<?php echo site_url(); ?>/clientes/guardarCliente" method="post" id="frm_nuevo_cliente">
      <table class="table">
        <tr>
          <td><label for="">Imganen:</label></td>
          <td><input type="file" name="imagen_cli" id="imagen_cli" class="form-control"
          accept="image/jpeg" value="" ></td>
        </tr>
        <tr>
          <td><label for="">Cedula: </label></td>
          <td><input type="number" name="cedula_cli" id="cedula_cli" class="form-control"
          value="" placeholder="Ingrese su cedula" required></td>
        </tr>
        <tr>
          <td><label for="">Nombres: </label></td>
          <td><input type="text" name="nombre_cli" id="nombre_cli" class="form-control"
          value="" placeholder="Ingrese su nombre" required></td>
        </tr>

        <tr>
          <td><label for="">Apellidos:</label></td>
          <td><input type="text" name="apellido_cli" id="apellido_cli" class="form-control"
        value="" placeholder="Ingrese su apellido" required autocomplete="off"></td>
        </tr>

        <tr>
            <td><label for="">Direccion Exacta: </label></td>
            <td>  <input type="direccion" name="direccion_me"  id="direccion_me" class="form-control bg-light border-0"
              value="" placeholder="Ingrese la direccion exacta(Barrio,Calles)" required ></td>

          </tr>
        <tr>
          <td><label for="">Género:</label></td>
          <td><select  name="genero_cli" id="genero_cli" class="form-control" value="" placeholder="Escoja su genero" required autocomplete="off">
          <option value="">--Seleccione----</option>
          <option value="1">Masculino</option>
          <option value="2">Femenino</option>
        </select>
        </td>
        </tr>


      </table>


      <button type="submit" name="button" class="btn btn-success">
        <i class="glyphicon glyphicon-ok"></i>
        Guardar</button>
      <a href="<?php echo site_url(); ?>/clientes/index" class="btn btn-danger">
        <i class="glyphicon glyphicon-remove"></i>
        Cancelar</a>
    </form>
  </div>
  <div class="col-md-3">

  </div>
</div>
</div>
<script type="text/javascript">
  $("#frm_nuevo_cliente").validate({
    rules:{
      cedula_cli:{
        required:true,
        digits:true,
        maxlength:10,
        minlength:10,
        remote:{
                 url:"<?php echo site_url('clientes/validarCedulaExistente'); ?>",
                   data:{
                     "cedula_cli":function(){
                       return $("#cedula_cli").val();
                     }
                   },
                   type:"post"
               }
      },
      nombre_cli:{
        required:true
      },
      apellido_cli:{
        required:true
      },
      direccion_me:{
        required:true
      },
      genero_cli:{
        required:true
      }
    },
    messages:{
      cedula_cli:{
        required:"<br>Por favor ingrese la cedula",
        digits:"<br>Por favor ingrese solo numeros",
        maxlength:"<br>Por favor ingrese 10 digitos",
        minlength:"<br>Por favor ingrese 10 digitos",
        remote:"este numeor de cedula esta en uso"
      },
      nombre_cli:{
        required:"Por favor ingrese los nombres"
      },
      apellido_cli:{
        required:"Por favor ingrese los apellidos"
      },
      direccion_me:{
        required:"Por favor ingrese la direccion"
      },
      genero_cli:{
        required:"Por favor escoga el genero"
      }
    }
  });
</script>

<script>
  $( function() {
    var availableTags = [
			<?php if ($listadoApellidoClientes): ?>
				<?php foreach ($listadoApellidoClientes->result() as $apellidoTemporal): ?>
					'<?php echo $apellidoTemporal->apellido_cli; ?>',
				<?php endforeach; ?>
			<?php endif; ?>
    ];
    $( "#apellido_cli" ).autocomplete({
      source: availableTags
    });
  } );
  </script>


  <script type="text/javascript">
    $(document).ready(function (){
      $("#imagen_cli").fileinput({
        language:'es'
      });
    });
  </script>
  
  <script>
  $( function() {
    var availableTags = [
      <?php if ($listadoDireccionesMedico): ?>
        <?php foreach ($listadoDireccionesMedico->result() as $direccionTemporal): ?>
          '<?php echo $direccionTemporal->direccion_me; ?>',
        <?php endforeach; ?>
      <?php endif; ?>
    ];
    $( "#direccion_me" ).autocomplete({
      source: availableTags
    });
  } );
  </script>
