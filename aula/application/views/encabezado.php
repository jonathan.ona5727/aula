<!DOCTYPE html>

<html lang="es" dir="ltr">
	<head>
		<meta charset="utf-8">
		<meta name="description" content="Examen">
		<title>Aula</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel='SHORTCUT ICON' href='https://www.unicef.org/lac/sites/unicef.org.lac/files/styles/press_release_feature/public/_DSC9171_WithText_v01-1.jpg?itok=zg2s4ZDd' type='image/x-icon' />
		<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
		<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/librerias/bootstrap3/css/bootstrap.css">
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/librerias/bootstrap3/js/bootstrap.js"></script>
		<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>



    <link rel="stylesheet" href="//cdn.datatables.net/1.11.4/css/jquery.dataTables.min.css">
        <script type="text/javascript" src="//cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>



		<?php if ($this->session->flashdata('confirmacion')): ?>
			<script type="text/javascript">
				$(document).ready(function(){
					Swal.fire(
						'CONFIRMACÍON',
						'<?php echo $this->session->flashdata('confirmacion') ?>',
						'success'
					);
				});
			</script>
		<?php endif; ?>


  <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

<!-- Importando  file -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.2.7/js/fileinput.min.js" integrity="sha512-CCLv901EuJXf3k0OrE5qix8s2HaCDpjeBERR2wVHUwzEIc7jfiK9wqJFssyMOc1lJ/KvYKsDenzxbDTAQ4nh1w==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.2.7/css/fileinput.min.css" integrity="sha512-qPjB0hQKYTx1Za9Xip5h0PXcxaR1cRbHuZHo9z+gb5IgM6ZOTtIH4QLITCxcCp/8RMXtw2Z85MIZLv6LfGTLiw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.2.7/js/locales/es.min.js" integrity="sha512-Mu+FX6b1AzpD49KmO13T8uLDpHK8M1vgZ75094jEP4KV0j59xJFfRz/SP0QIr/9dmHc4yIUGEFcnTU3uhHJaJg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>


  <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" integrity="sha512-uto9mlQzrs59VwILcLiRYeLKPPbS/bT71da/OEBYEwcdNUk8jYIy+D176RYoop1Da+f9mvkYrmj5MCLZWEtQuA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" integrity="sha512-aOG0c6nPNzGk+5zjwyJaoRUgCdOrfSDhmMID2u4+OIslr0GjpLKo7Xm0Ao3xmpM4T8AmIouRkqwj1nrdVsLKEQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <!--hacerla aplicaciom descargable-->
<meta name="theme-color" content="#7DC22B">
<meta name="MobileOptimized" content="width">
<meta name="HandheldFriendly" content="true">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<link rel="shortcut icon" type="image/png" href="./img/accesor1.png">
<link rel="apple-touch-icon" href="./accesor1.png">
<link rel="apple-touch-startup-image" href="./accesor1.png">
<meta name="apple-mobile-web-app-title" content="Sistema de informacion de multimedia">
<link rel="manifest" href="<?php echo base_url(); ?>manifest.json">
<script type="text/javascript" src="<?php echo base_url('script.js'); ?>"></script>
  <!-- Importacion de push js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/push.js/1.0.8/push.js" integrity="sha512-x0GVeKL5uwqADbWOobFCUK4zTI+MAXX/b7dwpCVfi/RT6jSLkSEzzy/ist27Iz3/CWzSvvbK2GBIiT7D4ZxtPg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script type="text/javascript">
    function	lanzarNotificacion(){
      Push.Permission.request(); //Solicitar Permiso
      //Creando Notificación Push
      Push.create('NOTIFICACIÓN EXAMEN',{
        body:'Saludos Haciendo el Examen',
        icon:'<?php echo base_url('/img/favicon.png'); ?>',
        timeout:10000, //tiempo de visibilidad en milisegundos
        vibrate: [100,100,100],
        onClick:function(){
          alert("ok");
        }
      });

    }
</script>
	</head>
	<body style="background-color:#79A9D8;">
    <button type="button" name="button" class="btn btn-primary"
													onclick="lanzarNotificacion()">
													Recargar Pagina
												</button></a>
		<div class="row">
		  <div class="col-md-2 text-center">
		  </div>
		  <div class="col-md-8">
				<nav class="">


					<form class="navbar-form navbar-right"><br>
						<?php if ($this->session->flashdata('confirmacion')):?>
      	<script type="text/javascript">
      	$(document).ready(function(){
      		Swal.fire(
      		  'CONFIRMACION!',
      			'<?php echo
      			$this->session->flashdata('confirmacion');?>',

      			'success'
      		//  'warning'//icono de advertencia

      		);
      	});
      	</script>
      <?php endif; ?>


            <?php if ($this->session->flashdata('eliminacion')):?>
            <script type="text/javascript">
            Swal.fire({
            	title: 'Are you sure?',
            	text: "You won't be able to revert this!",
            	icon: 'warning',
            	showCancelButton: true,
            	confirmButtonColor: '#3085d6',
            	cancelButtonColor: '#d33',
            	confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
            	if (result.isConfirmed) {
            		Swal.fire(
            			'Deleted!',
            			'Your file has been deleted.',
            			'success'
            		)
            	}
            })

            </script>
            <?php endif; ?>
            <?php if ($this->session->flashdata('error')):?>
    		<script type="text/javascript">
    		$(document).ready(function(){
    			Swal.fire(
    				'ERROR!',
    				'<?php echo
    				$this->session->flashdata('error');?>',
    				'error'

    			);
    		});
    		</script>
    	<?php endif; ?>
      <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

    <!-- Importando  file -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.2.7/js/fileinput.min.js" integrity="sha512-CCLv901EuJXf3k0OrE5qix8s2HaCDpjeBERR2wVHUwzEIc7jfiK9wqJFssyMOc1lJ/KvYKsDenzxbDTAQ4nh1w==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.2.7/css/fileinput.min.css" integrity="sha512-qPjB0hQKYTx1Za9Xip5h0PXcxaR1cRbHuZHo9z+gb5IgM6ZOTtIH4QLITCxcCp/8RMXtw2Z85MIZLv6LfGTLiw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
      <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.2.7/js/locales/es.min.js" integrity="sha512-Mu+FX6b1AzpD49KmO13T8uLDpHK8M1vgZ75094jEP4KV0j59xJFfRz/SP0QIr/9dmHc4yIUGEFcnTU3uhHJaJg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>


      <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" integrity="sha512-uto9mlQzrs59VwILcLiRYeLKPPbS/bT71da/OEBYEwcdNUk8jYIy+D176RYoop1Da+f9mvkYrmj5MCLZWEtQuA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" integrity="sha512-aOG0c6nPNzGk+5zjwyJaoRUgCdOrfSDhmMID2u4+OIslr0GjpLKo7Xm0Ao3xmpM4T8AmIouRkqwj1nrdVsLKEQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
      <!--hacerla aplicaciom descargable-->
    <meta name="theme-color" content="#7DC22B">
    <meta name="MobileOptimized" content="width">
    <meta name="HandheldFriendly" content="true">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <link rel="shortcut icon" type="image/png" href="./img/aula.png">
    <link rel="apple-touch-icon" href="./aula.png">
    <link rel="apple-touch-startup-image" href="./aula.png">
    <meta name="apple-mobile-web-app-title" content="Sistema de informacion de multimedia">
    <link rel="manifest" href="<?php echo base_url(); ?>manifest.json">
    <script type="text/javascript" src="<?php echo base_url('script.js'); ?>"></script>
      <!-- Importacion de push js -->




			<a href="<?php echo site_url(); ?>" class="btn btn-light" style=" background:white;">Inicio</a>

			<a href="<?php echo site_url(); ?>/clientes/index" class="btn btn-light" style=" background:white;">Gestión de Clientes</a>
			<a href="<?php echo site_url(); ?>/GestionesVehiculo/gestionVehiculos" class="btn btn-light" style=" background:white;">Gestión de Vehiculo</a>
			<a href="<?php echo site_url(); ?>/GestionesServicio/gestionServicios" class="btn btn-light" style=" background:white;">Gestión de Horario</a>
			<a href="<?php echo site_url(); ?>/servicios/index" class="btn btn-light" style=" background:white;">Gestión de Pago</a>
				<a href="<?php echo site_url(); ?>/reportes/index" class="btn btn-light" style=" background:white;">Reportes</a>

      </form>

				</nav>
		  </div>
		  <div class="col-md-2 text-center">
		  </div>
		</div>
		<div class="row">
		<div class="col-md-2 text-center">
		</div>
		<div class="col-md-8">
			<hr style="height:1px;">
			</div>
			<div class="col-md-2 text-center">
			</div>
		</div>
