<?php
  class Servicio extends CI_Model{
    //funcion para inserta nuevo clientes
    public function insertar($datosServicio){
      return $this->db->insert('servicio',$datosServicio);
    }
    //funcion para consultar datos de BDD
    public function obtenerTodos(){
      $this->db->join('cliente','cliente.id_cli=servicio.fk_id_cli');
      $query=$this->db->get('servicio');
      if ($query->num_rows()>0) {
        return $query;//cuando si hay registros en la BDD
      }else {
        return false;//cuando no hay registros
      }
    }
    public function obtenerPorId($id){
      $this->db->where('id_ser',$id);
      $query=$this->db->get('servicio');
      if ($query->num_rows()>0) {
        return $query->row();//cuando si hay registros en la BDD
      }else {
        return false;//cuando no hay registros
      }
    }
    //metodo para eliminar cliente recibiendo
    //como parametro su idea
    //funcion para eliminar clientes
  public function eliminarPorId($id_ser)
  {
    $this->db->where('id_ser',$id_ser);
    return $this->db->delete('servicio');
  }
  //funcion para procesar la actualizacion del clientes
  public function actualizar($id,$datosServicio){
    $this->db->where('id_ser',$id);
    return $this->db->update('servicio',$datosServicio);
  }
  }
 ?>
