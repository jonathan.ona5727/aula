<?php
class GestionesServicio extends CI_Controller{

    public function __construct(){
      parent::__construct();
      $this->load->database();
      $this->load->library('grocery_CRUD');
    }

    public function gestionServicios(){
      $servicios=new grocery_CRUD();
      $servicios->set_relation('fk_id_cli','cliente','Nombre: {nombre_cli}');
      $servicios->set_relation('fk_id_veh','vehiculo','Placa: {placa_veh}');
      $servicios->set_table('servicio');
      $servicios->set_language('spanish');
      $servicios->set_theme('datatables');
      $servicios->field_type('horaIngreso_ser','Check_in_time');
      $servicios->field_type('horaSalida_ser', array($this, '_cb_date'));

      $servicios->display_as('id_ser','ID');
      $servicios->display_as('fk_id_cli','Nombre');
      $servicios->display_as('fk_id_veh','Placa');
      $servicios->display_as('horaIngreso_ser','Hora de Ingreso');
      $servicios->display_as('horaSalida_ser','Hora de Salida');
      $output=$servicios->render();
      $this->load->view('encabezado');
      $this->load->view('gestionesVehiculo/gestionVehiculos',$output);
      $this->load->view('pie');
    }

    public function _cb_date($value, $row)

    {

      $my_time = horaSalida_ser("H:i:s",strtotime($value));

      return '<div class="text-left">' . $my_time . '</div>';

    }




  }
 ?>
