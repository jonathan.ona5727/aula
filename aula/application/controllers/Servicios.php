<?php
class Servicios extends CI_Controller{

  public function __construct(){
    parent::__construct();
    $this->load->model('servicio');
  }
  public function index()
  {
    $data["listadoServicios"]=$this->servicio->obtenerTodos();
    $this->load->view('encabezado');
    $this->load->view('servicios/index',$data);
    $this->load->view('pie');
  }
  public function nuevo(){
    $this->load->view('encabezado');
    $this->load->view('servicios/nuevo');
    $this->load->view('pie');
  }
  public function guardarServicio(){
      // $placa=$this->input->post('fk_id_veh');
      $horaIngreso=$this->input->post('nombre_cli');
      $horaIngreso=$this->input->post('horaIngreso_ser');
      $horaSalida=$this->input->post('horaSalida_ser');

      $datosNuevoServicio=array(
        // "fk_id_veh"=>$placa,
        "nombre_cli"=>$nombre_cli,
        "horaIngreso_ser"=>$horaIngreso,
        "horaSalida_ser"=>$horaSalida,
      );
      if ($this->servicio->insertar($datosNuevoServicio)) {
        $this->session->set_flashdata("confirmacion","Servicio registrado exitosamente");
        redirect('servicios/index');
      }else {
        echo "Servicio no guardado";
      }
  }
  public function eliminarServicio($id){

    if ($this->servicio->eliminarPorId($id)) {
      $this->session->set_flashdata("confirmacion","Servicio eliminado exitosamente");
      redirect('servicios/index');
    }else {
      echo 'Error al eliminar';
    }
  }
  public function editar($id){
    $data['servicioEditar']=$this->servicio->obtenerPorId($id);
    $this->load->view('encabezado');
    $this->load->view('servicios/editar',$data);
    $this->load->view('pie');
  }
  public function actualizarServicio(){
    $id_ser=$this->input->post('id_ser');
    $horaIngreso=$this->input->post('horaIngreso_ser');
    $horaSalida=$this->input->post('horaSalida_ser');
    $datosEditados=array(
      // "fk_id_veh"=>$this->input->post('fk_id_veh'),
      "horaIngreso_ser"=>$horaIngreso,
      "horaSalida_ser"=>$horaSalida
    );
    if($this->servicio->actualizar($id_ser,$datosEditados)){
      $this->session->set_flashdata("confirmacion","Servicio actualizado exitosamente");
      redirect('servicios/index');
    }else{
      echo "Error al actualizar";
    }
  }
  }
?>
