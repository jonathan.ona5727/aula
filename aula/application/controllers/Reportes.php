<?php
class Reportes extends CI_Controller{
  public function __construct(){
    parent::__construct();
    $this->load->model('cliente');
  }
  public function index(){
    $data["listadoGeneros"]=$this->cliente->obtenerTodosPorGenero();
    $data["cliente"]=$this->cliente->obtenerTodos();
    $this->load->view("encabezado");
    $this->load->view("reportes/index",$data);
    $this->load->view("pie");
  }
}
