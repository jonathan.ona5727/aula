<?php
class Clientes extends CI_Controller{

  public function __construct(){
    parent::__construct();
    $this->load->model('cliente');
  }
  public function index()
  {
    $data["listadoDireccionesMedico"]=$this->cliente->obtenerTodosDireccionesConteoMedico();
    $data["listadoApellidoClientes"]=$this->cliente->obtenerTodosDireccionesConteo();
    $data["listadoClientes"]=$this->cliente->obtenerTodos();
    $this->load->view('encabezado');
    $this->load->view('clientes/index',$data);
    $this->load->view('pie');
  }
  public function nuevo(){
    $this->load->view('encabezado');
    $this->load->view('clientes/nuevo');
    $this->load->view('pie');
  }
  public function guardarCliente(){
    // inicio proceso de subida de arcgivos
        $id_imagen=$this->input->post("id_imagen");
      // ruta de subid de archivo
        $config['upload_path']=APPPATH.'../uploads/';
        // tipo de archivo permitido
        $config['allowed_types']='jpeg|jpg|png';
        // definir peso maximo de subida
        $config['max_size']=5*1024;
        // creamos un nombre aleatorio
        $nomnre_aleatorio="Cliente_".time()*rand(100,1000);
        // asignando el nombre al archivo subido
        $config['file_name']=$nomnre_aleatorio;
        // Cargando la libreria upload
        $this->load->library('upload',$config);
        if ($this->upload->do_upload("imagen_cli")) {
          // capturando informacion del archivo
          $dataArchivoSubido=$this->upload->data();
          // obteniendo el nombre del archivo
          $nombre_archivo_subido=$dataArchivoSubido["file_name"];
        }else {
          // cuando el archivo no se sube el nombre queda vacio
          $nombre_archivo_subido="";
        }
      // fin proceso de subida de archivos
      $imagen=$this->input->post('imagen_cli');
      $cedula=$this->input->post('cedula_cli');
      $nombre=$this->input->post('nombre_cli');
      $apellido=$this->input->post('apellido_cli');
      $genero=$this->input->post('genero_cli');
      $direccion=$this->input->post('direccion_me');
      $datosNuevoCliente= array("imagen_cli"=>$nombre_archivo_subido,
        "imagen_cli"=>$imagen,
        "cedula_cli"=>$cedula,
        "nombre_cli"=>$nombre,
        "apellido_cli"=>$apellido,
        "genero_cli"=>$genero,
        "direccion_me"=>$direccion,
);
      if ($this->cliente->insertar($datosNuevoCliente)) {
        $this->session->set_flashdata("confirmacion","Cliente registrado exitosamente");
        redirect('clientes/index');
      }else {
        echo "Cliente no guardado";
      }
  }
  public function eliminarCliente($id){

    if ($this->cliente->eliminarPorId($id)) {
      $this->session->set_flashdata("confirmacion","Cliente eliminado exitosamente");
      redirect('clientes/index');
    }else {
      echo 'Error al eliminar';
    }
  }
  public function editar($id){
    $data['clienteEditar']=$this->cliente->obtenerPorId($id);
    $this->load->view('encabezado');
    $this->load->view('clientes/editar',$data);
    $this->load->view('pie');
  }
  public function actualizarCliente(){
    $id_cli=$this->input->post('id_cli');
    $datosEditados=array(
      "imagen_cli"=>$this->input->post('imagen_cli'),
      "cedula_cli"=>$this->input->post('cedula_cli'),
      "nombre_cli"=>$this->input->post('nombre_cli'),
      "apellido_cli"=>$this->input->post('apellido_cli'),
      "direccion_me"=>$this->input->post('direccion_me'),
      "genero_cli"=>$this->input->post('genero_cli'),
    );
    if($this->cliente->actualizar($id_cli,$datosEditados)){
      $this->session->set_flashdata("confirmacion","Cliente actualizado exitosamente");
      redirect('clientes/index');
    }else{
      echo "Error al actualizar";
    }
  }
  public function validarCedulaExistente(){
        $cedula_cli=$this->input->post('cedula_cli');
        $clienteExistente=$this->cliente->consultarClientePorCedula($cedula_cli);
        if($clienteExistente){
          //print_r($clienteExistente);
        // echo json_encode($clienteExistente);
        echo json_encode(FALSE);
        }else{
        echo json_encode(TRUE);
        }
    }
  }
?>
