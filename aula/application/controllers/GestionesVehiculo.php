<?php
class GestionesVehiculo extends CI_Controller{

    public function __construct(){
      parent::__construct();
      $this->load->database();
      $this->load->library('grocery_CRUD');
    }

    public function gestionVehiculos(){
      $vehiculos=new grocery_CRUD();
      $vehiculos->set_relation('fk_id_cli','cliente','Nombre: {nombre_cli}--Apellido: {apellido_cli}');
      $vehiculos->set_table('vehiculo');
      $vehiculos->set_language('spanish');
      $vehiculos->set_theme('datatables');
      $vehiculos->set_field_upload('foto_veh','uploads');

      $vehiculos->display_as('id_veh','ID');
      $vehiculos->display_as('fk_id_cli','Clientes');
      $vehiculos->display_as('placa_veh','Placa');
      $vehiculos->display_as('foto_veh','Foto');
      $vehiculos->required_fields('id_veh','fk_id_cli','placa_veh','foto_veh');
      $output=$vehiculos->render();
      $this->load->view('encabezado');
      $this->load->view('gestionesVehiculo/gestionVehiculos',$output);
      $this->load->view('pie');
    }
  }
 ?>
